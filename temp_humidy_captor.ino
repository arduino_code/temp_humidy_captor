#include <LiquidCrystal.h>
#include <DHT_Async.h>

#define DHT_SENSOR_TYPE DHT_TYPE_11

static const int DHT_SENSOR_PIN = 2;
DHT_Async dht_sensor( DHT_SENSOR_PIN, DHT_SENSOR_TYPE );

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
}

static bool measure_environment( float *temperature, float *humidity )
{
  static unsigned long measurement_timestamp = millis( );

  /* Measure once every four seconds. */
  if( millis( ) - measurement_timestamp > 4000 )
  {
    if( dht_sensor.measure( temperature, humidity ) == true )
    {
      measurement_timestamp = millis( );
      return true;
    }
  }

  return false;
}


void loop() {
  float temperature;
  float humidity;

  if( measure_environment( &temperature, &humidity ) == true )
  {
    lcd.setCursor(0, 0);
    lcd.print("Temp = ");
    lcd.print(temperature);

    Serial.println(temperature);
    Serial.println(humidity);

    lcd.setCursor(0, 1);
    lcd.print("Humidity = ");
    lcd.print(humidity);
  }
}
